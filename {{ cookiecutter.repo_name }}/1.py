import os
import git
import requests
import getpass

# GitLab API endpoint for creating a new project
gitlab_api_url = 'https://gitlab.com/api/v4/projects'

# Input project details
project_name = "{{cookiecutter.repo_name}}"
# project_description = input("Enter the project description: ")
visibility = input("Set the project visibility (public/private): ").lower()
gitlab_token = getpass.getpass("Enter your GitLab personal access token (without displaying): ")

# Create a new project on GitLab
headers = {
    'PRIVATE-TOKEN': gitlab_token,
}
data = {
    'name': project_name,
    # 'description': project_description,
    'visibility': visibility,
}

response = requests.post(gitlab_api_url, headers=headers, data=data)

if response.status_code == 201:
    print(f"Project '{project_name}' successfully created on GitLab.")
else:
    print("Failed to create the project. Check your access token and settings.")

# Initialize a local Git repository (if not already initialized)
if not os.path.exists(".git"):
    os.system("git init")

# Set the remote origin URL to the newly created GitLab repository
remote_url = response.json()['http_url_to_repo']
os.system(f"git remote add origin {remote_url}")

# Push your project to the remote repository on GitLab
os.system("git add .")
os.system("git commit -m 'Initial commit'")
os.system("git push -u origin master")
